// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1-devel
// 	protoc        v3.15.8
// source: address_book.proto

package address_pb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type AddressBook struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ContactPerson string `protobuf:"bytes,2,opt,name=contact_person,json=contactPerson,proto3" json:"contact_person,omitempty"`
	Whatsapp      string `protobuf:"bytes,3,opt,name=whatsapp,proto3" json:"whatsapp,omitempty"`
	PhoneNumber   string `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	Latitude      string `protobuf:"bytes,5,opt,name=latitude,proto3" json:"latitude,omitempty"`
	Longitude     string `protobuf:"bytes,6,opt,name=longitude,proto3" json:"longitude,omitempty"`
	Address       string `protobuf:"bytes,7,opt,name=address,proto3" json:"address,omitempty"`
	ProvinceId    string `protobuf:"bytes,8,opt,name=province_id,json=provinceId,proto3" json:"province_id,omitempty"`
	RegencyId     string `protobuf:"bytes,9,opt,name=regency_id,json=regencyId,proto3" json:"regency_id,omitempty"`
	DistrictId    string `protobuf:"bytes,10,opt,name=district_id,json=districtId,proto3" json:"district_id,omitempty"`
	Zipcode       string `protobuf:"bytes,11,opt,name=zipcode,proto3" json:"zipcode,omitempty"`
	CreatedAt     string `protobuf:"bytes,12,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt     string `protobuf:"bytes,13,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	UserId        string `protobuf:"bytes,14,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
}

func (x *AddressBook) Reset() {
	*x = AddressBook{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AddressBook) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AddressBook) ProtoMessage() {}

func (x *AddressBook) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AddressBook.ProtoReflect.Descriptor instead.
func (*AddressBook) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{0}
}

func (x *AddressBook) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *AddressBook) GetContactPerson() string {
	if x != nil {
		return x.ContactPerson
	}
	return ""
}

func (x *AddressBook) GetWhatsapp() string {
	if x != nil {
		return x.Whatsapp
	}
	return ""
}

func (x *AddressBook) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *AddressBook) GetLatitude() string {
	if x != nil {
		return x.Latitude
	}
	return ""
}

func (x *AddressBook) GetLongitude() string {
	if x != nil {
		return x.Longitude
	}
	return ""
}

func (x *AddressBook) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *AddressBook) GetProvinceId() string {
	if x != nil {
		return x.ProvinceId
	}
	return ""
}

func (x *AddressBook) GetRegencyId() string {
	if x != nil {
		return x.RegencyId
	}
	return ""
}

func (x *AddressBook) GetDistrictId() string {
	if x != nil {
		return x.DistrictId
	}
	return ""
}

func (x *AddressBook) GetZipcode() string {
	if x != nil {
		return x.Zipcode
	}
	return ""
}

func (x *AddressBook) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *AddressBook) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *AddressBook) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

type StoreAddressBookRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AddressBook *AddressBook `protobuf:"bytes,1,opt,name=address_book,json=addressBook,proto3" json:"address_book,omitempty"`
}

func (x *StoreAddressBookRequest) Reset() {
	*x = StoreAddressBookRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StoreAddressBookRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StoreAddressBookRequest) ProtoMessage() {}

func (x *StoreAddressBookRequest) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StoreAddressBookRequest.ProtoReflect.Descriptor instead.
func (*StoreAddressBookRequest) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{1}
}

func (x *StoreAddressBookRequest) GetAddressBook() *AddressBook {
	if x != nil {
		return x.AddressBook
	}
	return nil
}

type UpdatedAddressBookResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Result string `protobuf:"bytes,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *UpdatedAddressBookResponse) Reset() {
	*x = UpdatedAddressBookResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatedAddressBookResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatedAddressBookResponse) ProtoMessage() {}

func (x *UpdatedAddressBookResponse) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatedAddressBookResponse.ProtoReflect.Descriptor instead.
func (*UpdatedAddressBookResponse) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{2}
}

func (x *UpdatedAddressBookResponse) GetResult() string {
	if x != nil {
		return x.Result
	}
	return ""
}

type DeleteAddressBookRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AddressBookId string `protobuf:"bytes,1,opt,name=address_book_id,json=addressBookId,proto3" json:"address_book_id,omitempty"`
}

func (x *DeleteAddressBookRequest) Reset() {
	*x = DeleteAddressBookRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteAddressBookRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteAddressBookRequest) ProtoMessage() {}

func (x *DeleteAddressBookRequest) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteAddressBookRequest.ProtoReflect.Descriptor instead.
func (*DeleteAddressBookRequest) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{3}
}

func (x *DeleteAddressBookRequest) GetAddressBookId() string {
	if x != nil {
		return x.AddressBookId
	}
	return ""
}

type FetchAddressBookRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId string `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
}

func (x *FetchAddressBookRequest) Reset() {
	*x = FetchAddressBookRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FetchAddressBookRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FetchAddressBookRequest) ProtoMessage() {}

func (x *FetchAddressBookRequest) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FetchAddressBookRequest.ProtoReflect.Descriptor instead.
func (*FetchAddressBookRequest) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{4}
}

func (x *FetchAddressBookRequest) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

type FetchAddressBookResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AddressBook *AddressBook `protobuf:"bytes,1,opt,name=address_book,json=addressBook,proto3" json:"address_book,omitempty"`
}

func (x *FetchAddressBookResponse) Reset() {
	*x = FetchAddressBookResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FetchAddressBookResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FetchAddressBookResponse) ProtoMessage() {}

func (x *FetchAddressBookResponse) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FetchAddressBookResponse.ProtoReflect.Descriptor instead.
func (*FetchAddressBookResponse) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{5}
}

func (x *FetchAddressBookResponse) GetAddressBook() *AddressBook {
	if x != nil {
		return x.AddressBook
	}
	return nil
}

type ShowAddressBookRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AddressBookId string `protobuf:"bytes,1,opt,name=address_book_id,json=addressBookId,proto3" json:"address_book_id,omitempty"`
}

func (x *ShowAddressBookRequest) Reset() {
	*x = ShowAddressBookRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ShowAddressBookRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ShowAddressBookRequest) ProtoMessage() {}

func (x *ShowAddressBookRequest) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ShowAddressBookRequest.ProtoReflect.Descriptor instead.
func (*ShowAddressBookRequest) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{6}
}

func (x *ShowAddressBookRequest) GetAddressBookId() string {
	if x != nil {
		return x.AddressBookId
	}
	return ""
}

type ShowAddressBookResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AddressBook *AddressBook `protobuf:"bytes,1,opt,name=address_book,json=addressBook,proto3" json:"address_book,omitempty"`
}

func (x *ShowAddressBookResponse) Reset() {
	*x = ShowAddressBookResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_address_book_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ShowAddressBookResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ShowAddressBookResponse) ProtoMessage() {}

func (x *ShowAddressBookResponse) ProtoReflect() protoreflect.Message {
	mi := &file_address_book_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ShowAddressBookResponse.ProtoReflect.Descriptor instead.
func (*ShowAddressBookResponse) Descriptor() ([]byte, []int) {
	return file_address_book_proto_rawDescGZIP(), []int{7}
}

func (x *ShowAddressBookResponse) GetAddressBook() *AddressBook {
	if x != nil {
		return x.AddressBook
	}
	return nil
}

var File_address_book_proto protoreflect.FileDescriptor

var file_address_book_proto_rawDesc = []byte{
	0x0a, 0x12, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x62, 0x6f, 0x6f, 0x6b, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0a, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62,
	0x22, 0xa9, 0x03, 0x0a, 0x0b, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64,
	0x12, 0x25, 0x0a, 0x0e, 0x63, 0x6f, 0x6e, 0x74, 0x61, 0x63, 0x74, 0x5f, 0x70, 0x65, 0x72, 0x73,
	0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x63, 0x6f, 0x6e, 0x74, 0x61, 0x63,
	0x74, 0x50, 0x65, 0x72, 0x73, 0x6f, 0x6e, 0x12, 0x1a, 0x0a, 0x08, 0x77, 0x68, 0x61, 0x74, 0x73,
	0x61, 0x70, 0x70, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x77, 0x68, 0x61, 0x74, 0x73,
	0x61, 0x70, 0x70, 0x12, 0x21, 0x0a, 0x0c, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x5f, 0x6e, 0x75, 0x6d,
	0x62, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70, 0x68, 0x6f, 0x6e, 0x65,
	0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x1a, 0x0a, 0x08, 0x6c, 0x61, 0x74, 0x69, 0x74, 0x75,
	0x64, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6c, 0x61, 0x74, 0x69, 0x74, 0x75,
	0x64, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x6c, 0x6f, 0x6e, 0x67, 0x69, 0x74, 0x75, 0x64, 0x65, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x6c, 0x6f, 0x6e, 0x67, 0x69, 0x74, 0x75, 0x64, 0x65,
	0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18, 0x07, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x72,
	0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0a, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x72,
	0x65, 0x67, 0x65, 0x6e, 0x63, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x72, 0x65, 0x67, 0x65, 0x6e, 0x63, 0x79, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x64, 0x69,
	0x73, 0x74, 0x72, 0x69, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0a, 0x64, 0x69, 0x73, 0x74, 0x72, 0x69, 0x63, 0x74, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x7a,
	0x69, 0x70, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x7a, 0x69,
	0x70, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64,
	0x5f, 0x61, 0x74, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f,
	0x61, 0x74, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x64, 0x41, 0x74, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x0e,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x22, 0x55, 0x0a, 0x17,
	0x53, 0x74, 0x6f, 0x72, 0x65, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3a, 0x0a, 0x0c, 0x61, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x5f, 0x62, 0x6f, 0x6f, 0x6b, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e,
	0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x41, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x0b, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42,
	0x6f, 0x6f, 0x6b, 0x22, 0x34, 0x0a, 0x1a, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x22, 0x42, 0x0a, 0x18, 0x44, 0x65, 0x6c,
	0x65, 0x74, 0x65, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x26, 0x0a, 0x0f, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x5f, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d,
	0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x49, 0x64, 0x22, 0x32, 0x0a,
	0x17, 0x46, 0x65, 0x74, 0x63, 0x68, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f,
	0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72,
	0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49,
	0x64, 0x22, 0x56, 0x0a, 0x18, 0x46, 0x65, 0x74, 0x63, 0x68, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3a, 0x0a,
	0x0c, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x62, 0x6f, 0x6f, 0x6b, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62,
	0x2e, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x0b, 0x61, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x22, 0x40, 0x0a, 0x16, 0x53, 0x68, 0x6f,
	0x77, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x26, 0x0a, 0x0f, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x62,
	0x6f, 0x6f, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x61, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x49, 0x64, 0x22, 0x55, 0x0a, 0x17, 0x53,
	0x68, 0x6f, 0x77, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3a, 0x0a, 0x0c, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x5f, 0x62, 0x6f, 0x6f, 0x6b, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x0b, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f,
	0x6f, 0x6b, 0x32, 0xeb, 0x03, 0x0a, 0x12, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f,
	0x6f, 0x6b, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x5a, 0x0a, 0x0f, 0x53, 0x68, 0x6f,
	0x77, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x12, 0x22, 0x2e, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x53, 0x68, 0x6f, 0x77, 0x41, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x23, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x53, 0x68,
	0x6f, 0x77, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5f, 0x0a, 0x10, 0x53, 0x74, 0x6f, 0x72, 0x65, 0x41, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x12, 0x23, 0x2e, 0x61, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x53, 0x74, 0x6f, 0x72, 0x65, 0x41, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x26,
	0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x64, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x54, 0x0a, 0x11, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x12, 0x17, 0x2e, 0x61, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x42, 0x6f, 0x6f, 0x6b, 0x1a, 0x26, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70,
	0x62, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x61, 0x0a, 0x11,
	0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f,
	0x6b, 0x12, 0x24, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x44,
	0x65, 0x6c, 0x65, 0x74, 0x65, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x26, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x5f, 0x70, 0x62, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x5f, 0x0a, 0x10, 0x46, 0x65, 0x74, 0x63, 0x68, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42,
	0x6f, 0x6f, 0x6b, 0x12, 0x23, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62,
	0x2e, 0x46, 0x65, 0x74, 0x63, 0x68, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x6f, 0x6f,
	0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x24, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x5f, 0x70, 0x62, 0x2e, 0x46, 0x65, 0x74, 0x63, 0x68, 0x41, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x42, 0x6f, 0x6f, 0x6b, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x30, 0x01,
	0x42, 0x42, 0x5a, 0x40, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6d,
	0x34, 0x32, 0x39, 0x37, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2d, 0x62, 0x75, 0x66, 0x66, 0x65,
	0x72, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x66, 0x2f, 0x67, 0x6f, 0x2f, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x70, 0x62, 0x3b, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x5f, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_address_book_proto_rawDescOnce sync.Once
	file_address_book_proto_rawDescData = file_address_book_proto_rawDesc
)

func file_address_book_proto_rawDescGZIP() []byte {
	file_address_book_proto_rawDescOnce.Do(func() {
		file_address_book_proto_rawDescData = protoimpl.X.CompressGZIP(file_address_book_proto_rawDescData)
	})
	return file_address_book_proto_rawDescData
}

var file_address_book_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_address_book_proto_goTypes = []interface{}{
	(*AddressBook)(nil),                // 0: address_pb.AddressBook
	(*StoreAddressBookRequest)(nil),    // 1: address_pb.StoreAddressBookRequest
	(*UpdatedAddressBookResponse)(nil), // 2: address_pb.UpdatedAddressBookResponse
	(*DeleteAddressBookRequest)(nil),   // 3: address_pb.DeleteAddressBookRequest
	(*FetchAddressBookRequest)(nil),    // 4: address_pb.FetchAddressBookRequest
	(*FetchAddressBookResponse)(nil),   // 5: address_pb.FetchAddressBookResponse
	(*ShowAddressBookRequest)(nil),     // 6: address_pb.ShowAddressBookRequest
	(*ShowAddressBookResponse)(nil),    // 7: address_pb.ShowAddressBookResponse
}
var file_address_book_proto_depIdxs = []int32{
	0, // 0: address_pb.StoreAddressBookRequest.address_book:type_name -> address_pb.AddressBook
	0, // 1: address_pb.FetchAddressBookResponse.address_book:type_name -> address_pb.AddressBook
	0, // 2: address_pb.ShowAddressBookResponse.address_book:type_name -> address_pb.AddressBook
	6, // 3: address_pb.AddressBookService.ShowAddressBook:input_type -> address_pb.ShowAddressBookRequest
	1, // 4: address_pb.AddressBookService.StoreAddressBook:input_type -> address_pb.StoreAddressBookRequest
	0, // 5: address_pb.AddressBookService.UpdateAddressBook:input_type -> address_pb.AddressBook
	3, // 6: address_pb.AddressBookService.DeleteAddressBook:input_type -> address_pb.DeleteAddressBookRequest
	4, // 7: address_pb.AddressBookService.FetchAddressBook:input_type -> address_pb.FetchAddressBookRequest
	7, // 8: address_pb.AddressBookService.ShowAddressBook:output_type -> address_pb.ShowAddressBookResponse
	2, // 9: address_pb.AddressBookService.StoreAddressBook:output_type -> address_pb.UpdatedAddressBookResponse
	2, // 10: address_pb.AddressBookService.UpdateAddressBook:output_type -> address_pb.UpdatedAddressBookResponse
	2, // 11: address_pb.AddressBookService.DeleteAddressBook:output_type -> address_pb.UpdatedAddressBookResponse
	5, // 12: address_pb.AddressBookService.FetchAddressBook:output_type -> address_pb.FetchAddressBookResponse
	8, // [8:13] is the sub-list for method output_type
	3, // [3:8] is the sub-list for method input_type
	3, // [3:3] is the sub-list for extension type_name
	3, // [3:3] is the sub-list for extension extendee
	0, // [0:3] is the sub-list for field type_name
}

func init() { file_address_book_proto_init() }
func file_address_book_proto_init() {
	if File_address_book_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_address_book_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AddressBook); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_address_book_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StoreAddressBookRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_address_book_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatedAddressBookResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_address_book_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteAddressBookRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_address_book_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FetchAddressBookRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_address_book_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FetchAddressBookResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_address_book_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ShowAddressBookRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_address_book_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ShowAddressBookResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_address_book_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_address_book_proto_goTypes,
		DependencyIndexes: file_address_book_proto_depIdxs,
		MessageInfos:      file_address_book_proto_msgTypes,
	}.Build()
	File_address_book_proto = out.File
	file_address_book_proto_rawDesc = nil
	file_address_book_proto_goTypes = nil
	file_address_book_proto_depIdxs = nil
}
