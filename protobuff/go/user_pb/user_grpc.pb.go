// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.1.0
// - protoc             v3.15.8
// source: user.proto

package user_pb

import (
	context "context"
	system_pb "gitlab.com/m4297/proto-buffer/protobuff/go/system_pb"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// UserServiceClient is the client API for UserService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type UserServiceClient interface {
	FetchUser(ctx context.Context, in *system_pb.Empty, opts ...grpc.CallOption) (UserService_FetchUserClient, error)
	ShowUser(ctx context.Context, in *ShowUserRequest, opts ...grpc.CallOption) (*ShowUserResponse, error)
	StoreUser(ctx context.Context, in *StoreUserRequest, opts ...grpc.CallOption) (*StoreUserResponse, error)
	UpdateUser(ctx context.Context, in *UpdateUserRequest, opts ...grpc.CallOption) (*UpdateUserResponse, error)
	DeleteUser(ctx context.Context, in *DeleteUserRequest, opts ...grpc.CallOption) (*DeleteUserResponse, error)
	//Menambahkan address book yg sudah ada dari servis addressbook ke servis user
	AddAddressBook(ctx context.Context, in *AddAddressBookRequest, opts ...grpc.CallOption) (*AddAddressBookResponse, error)
}

type userServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewUserServiceClient(cc grpc.ClientConnInterface) UserServiceClient {
	return &userServiceClient{cc}
}

func (c *userServiceClient) FetchUser(ctx context.Context, in *system_pb.Empty, opts ...grpc.CallOption) (UserService_FetchUserClient, error) {
	stream, err := c.cc.NewStream(ctx, &UserService_ServiceDesc.Streams[0], "/user_pb.UserService/FetchUser", opts...)
	if err != nil {
		return nil, err
	}
	x := &userServiceFetchUserClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type UserService_FetchUserClient interface {
	Recv() (*User, error)
	grpc.ClientStream
}

type userServiceFetchUserClient struct {
	grpc.ClientStream
}

func (x *userServiceFetchUserClient) Recv() (*User, error) {
	m := new(User)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *userServiceClient) ShowUser(ctx context.Context, in *ShowUserRequest, opts ...grpc.CallOption) (*ShowUserResponse, error) {
	out := new(ShowUserResponse)
	err := c.cc.Invoke(ctx, "/user_pb.UserService/ShowUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) StoreUser(ctx context.Context, in *StoreUserRequest, opts ...grpc.CallOption) (*StoreUserResponse, error) {
	out := new(StoreUserResponse)
	err := c.cc.Invoke(ctx, "/user_pb.UserService/StoreUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) UpdateUser(ctx context.Context, in *UpdateUserRequest, opts ...grpc.CallOption) (*UpdateUserResponse, error) {
	out := new(UpdateUserResponse)
	err := c.cc.Invoke(ctx, "/user_pb.UserService/UpdateUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) DeleteUser(ctx context.Context, in *DeleteUserRequest, opts ...grpc.CallOption) (*DeleteUserResponse, error) {
	out := new(DeleteUserResponse)
	err := c.cc.Invoke(ctx, "/user_pb.UserService/DeleteUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) AddAddressBook(ctx context.Context, in *AddAddressBookRequest, opts ...grpc.CallOption) (*AddAddressBookResponse, error) {
	out := new(AddAddressBookResponse)
	err := c.cc.Invoke(ctx, "/user_pb.UserService/AddAddressBook", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserServiceServer is the server API for UserService service.
// All implementations should embed UnimplementedUserServiceServer
// for forward compatibility
type UserServiceServer interface {
	FetchUser(*system_pb.Empty, UserService_FetchUserServer) error
	ShowUser(context.Context, *ShowUserRequest) (*ShowUserResponse, error)
	StoreUser(context.Context, *StoreUserRequest) (*StoreUserResponse, error)
	UpdateUser(context.Context, *UpdateUserRequest) (*UpdateUserResponse, error)
	DeleteUser(context.Context, *DeleteUserRequest) (*DeleteUserResponse, error)
	//Menambahkan address book yg sudah ada dari servis addressbook ke servis user
	AddAddressBook(context.Context, *AddAddressBookRequest) (*AddAddressBookResponse, error)
}

// UnimplementedUserServiceServer should be embedded to have forward compatible implementations.
type UnimplementedUserServiceServer struct {
}

func (UnimplementedUserServiceServer) FetchUser(*system_pb.Empty, UserService_FetchUserServer) error {
	return status.Errorf(codes.Unimplemented, "method FetchUser not implemented")
}
func (UnimplementedUserServiceServer) ShowUser(context.Context, *ShowUserRequest) (*ShowUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ShowUser not implemented")
}
func (UnimplementedUserServiceServer) StoreUser(context.Context, *StoreUserRequest) (*StoreUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method StoreUser not implemented")
}
func (UnimplementedUserServiceServer) UpdateUser(context.Context, *UpdateUserRequest) (*UpdateUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateUser not implemented")
}
func (UnimplementedUserServiceServer) DeleteUser(context.Context, *DeleteUserRequest) (*DeleteUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteUser not implemented")
}
func (UnimplementedUserServiceServer) AddAddressBook(context.Context, *AddAddressBookRequest) (*AddAddressBookResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddAddressBook not implemented")
}

// UnsafeUserServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to UserServiceServer will
// result in compilation errors.
type UnsafeUserServiceServer interface {
	mustEmbedUnimplementedUserServiceServer()
}

func RegisterUserServiceServer(s grpc.ServiceRegistrar, srv UserServiceServer) {
	s.RegisterService(&UserService_ServiceDesc, srv)
}

func _UserService_FetchUser_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(system_pb.Empty)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(UserServiceServer).FetchUser(m, &userServiceFetchUserServer{stream})
}

type UserService_FetchUserServer interface {
	Send(*User) error
	grpc.ServerStream
}

type userServiceFetchUserServer struct {
	grpc.ServerStream
}

func (x *userServiceFetchUserServer) Send(m *User) error {
	return x.ServerStream.SendMsg(m)
}

func _UserService_ShowUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ShowUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).ShowUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_pb.UserService/ShowUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).ShowUser(ctx, req.(*ShowUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_StoreUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StoreUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).StoreUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_pb.UserService/StoreUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).StoreUser(ctx, req.(*StoreUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_UpdateUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).UpdateUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_pb.UserService/UpdateUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).UpdateUser(ctx, req.(*UpdateUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_DeleteUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).DeleteUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_pb.UserService/DeleteUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).DeleteUser(ctx, req.(*DeleteUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_AddAddressBook_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddAddressBookRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).AddAddressBook(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_pb.UserService/AddAddressBook",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).AddAddressBook(ctx, req.(*AddAddressBookRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// UserService_ServiceDesc is the grpc.ServiceDesc for UserService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var UserService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "user_pb.UserService",
	HandlerType: (*UserServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ShowUser",
			Handler:    _UserService_ShowUser_Handler,
		},
		{
			MethodName: "StoreUser",
			Handler:    _UserService_StoreUser_Handler,
		},
		{
			MethodName: "UpdateUser",
			Handler:    _UserService_UpdateUser_Handler,
		},
		{
			MethodName: "DeleteUser",
			Handler:    _UserService_DeleteUser_Handler,
		},
		{
			MethodName: "AddAddressBook",
			Handler:    _UserService_AddAddressBook_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "FetchUser",
			Handler:       _UserService_FetchUser_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "user.proto",
}
