module gitlab.com/m4297/proto-buffer

go 1.16

require (
	github.com/envoyproxy/protoc-gen-validate v0.6.1
	github.com/iancoleman/strcase v0.2.0 // indirect
	github.com/lyft/protoc-gen-star v0.6.0 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/net v0.0.0-20211020060615-d418f374d309 // indirect
	golang.org/x/sys v0.0.0-20211020174200-9d6173849985 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.7 // indirect
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
